jQuery(document).ready(function () {
    ////    <vars>      ////
    var close = '<i class="fa fa-times hamodal-button hamodal-close" data-hamodal="hamodal-close" aria-hidden="true"></i>';//
    (function () {
      var span = document.createElement('span');
      
      span.className = 'fa';
      span.style.display = 'none';
      document.body.insertBefore(span, document.body.firstChild);
      
      function css(element, property) {
        return window.getComputedStyle(element, null).getPropertyValue(property);
      }
      
      if (css(span, 'font-family') !== 'FontAwesome' || css(span, 'font-family') !== 'fontawesome') {
        close = '<a href="#" class="hamodal-button hamodal-close" data-hamodal="hamodal-close">close</a>';
        //<a href="#" class="hamodal-button hamodal-close" data-hamodal="hamodal-close">close</a>';
      }
      document.body.removeChild(span);
    })();
    
    var lastTarget = null;
    ////    </vars>     ////
    ////    <init>      ////
    var $ = jQuery;
    //css
    var loc = $('script[src*="ha"');
    console.log(loc[0]);
    if(loc[0]){
        var url = $(loc[0]).attr('src');
        console.log(url);
        url = url.split('/');
        console.log(url);
        url.length = url.length - 1;
        console.log(url);
        url = url.join('/');
        console.log(url);
        url = url + '/hamodal.css';
        console.log(url);
        $('head').append('<link href="' + url + '" rel="stylesheet" type="text/css"></link>');
    }
    
  
    //create hamodal target
    $('body').append('<div id="hamodal-container" class="hamodal-container"></div>');
    $('#hamodal-container').append('<div class="hamodal-wrap"><div id="hamodal-content" class="hamodal-content"></div></div>');
    $('.hamodal-container').prepend(close);
    $('.hamodal-close').hide();
    var ytw = $('iframe[src*="youtube"]').attr('width',($('.hamodal-container').innerWidth() * .75 - 42 )+'px');
    $(ytw).attr('style', 'height:'+ ($(ytw).innerWidth() * .5625) +'px');
    ////    </init>     ////
    ////    <events>    ////
    // function that the elements will call with onclick
    $('.hamodal-button').click(function (event) {
      console.log(event);
      //we dont want the page jumping around on us
      event.preventDefault();
      //get the target from the caller ie. the link (.hamodal-button)
      var source = $(this).attr('data-hamodal');
  
      if (source === 'hamodal-close') {
  
        $('.hamodal-container').slideUp();
        $('.hamodal-container').removeClass('hamodal-active');
        $('.hamodal-content').html('');
        $('.hamodal-close').hide();
  
      } else {
        var sourceClass = '.' + source;
        var target = $(sourceClass).attr('data-hamodal');
        //console.log(target);
        $(target).removeAttr('style');
        if (target === 'hamodal') {
  
          $('.hamodal-container').slideDown();
          $('.hamodal-container').addClass('hamodal-active');
          $('.hamodal-content').html($(sourceClass).html());
          $('.hamodal-close').show();
  
        } else {
          target = '.' + target;
          console.log(target);
          $(target).html($(sourceClass).html());
          $(target).toggleClass('hamodal-show-container');
          
          if(!($(target).hasClass('hamodal-show-container'))){
            $(target).addClass('hamodal-hide-container');
          }else {
            $(target).removeClass('hamodal-hide-container');
          }
        }
      }
      lastTarget = source;
  
    });
    ////    </events>   ////
  
  });
  function killIframe(obj){
  obj.addEventListener("load", function(event){
        jQuery('.hamodal-container').slideUp();
        jQuery('.hamodal-container').removeClass('hamodal-active');
        jQuery('.hamodal-content').html('');
        jQuery('.hamodal-close').hide();
  
  });
  //jQuery();
    console.log('success');
  }