# hamodal

__hamodal module for drupal 7__

##description:
Hamodal is a drupal 7 module designed to 

##Use:
Active the module in Drupal 7. The module looks for a trigger & target which has the following relationship:

    <a href="#someID"  class="hamodal-button" data-hamodal="targetsClassName">click meh</a>

    <div id="someID" class="targetsClassName" data-hamodal="hamodal" style="display:none">
        any modal content here.
    </div>

##Installation:
Install like any other drupal module